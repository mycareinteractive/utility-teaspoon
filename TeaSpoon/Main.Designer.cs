﻿namespace TeaSpoon
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.fbdDirIn = new System.Windows.Forms.FolderBrowserDialog();
            this.fbdDirOut = new System.Windows.Forms.FolderBrowserDialog();
            this.btnDirIn = new System.Windows.Forms.Button();
            this.lblDirIn = new System.Windows.Forms.Label();
            this.btnDirOut = new System.Windows.Forms.Button();
            this.lblDirOut = new System.Windows.Forms.Label();
            this.tbDirIn = new System.Windows.Forms.TextBox();
            this.tbDirOut = new System.Windows.Forms.TextBox();
            this.tbStatus = new System.Windows.Forms.TextBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.btnScrape = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // fbdDirIn
            // 
            this.fbdDirIn.ShowNewFolderButton = false;
            // 
            // btnDirIn
            // 
            this.btnDirIn.Location = new System.Drawing.Point(197, 26);
            this.btnDirIn.Name = "btnDirIn";
            this.btnDirIn.Size = new System.Drawing.Size(75, 23);
            this.btnDirIn.TabIndex = 0;
            this.btnDirIn.Text = "Dir In";
            this.btnDirIn.UseVisualStyleBackColor = true;
            this.btnDirIn.Click += new System.EventHandler(this.btnDirIn_Click);
            // 
            // lblDirIn
            // 
            this.lblDirIn.AutoSize = true;
            this.lblDirIn.Location = new System.Drawing.Point(12, 32);
            this.lblDirIn.Name = "lblDirIn";
            this.lblDirIn.Size = new System.Drawing.Size(16, 13);
            this.lblDirIn.TabIndex = 1;
            this.lblDirIn.Text = "In";
            // 
            // btnDirOut
            // 
            this.btnDirOut.Location = new System.Drawing.Point(197, 63);
            this.btnDirOut.Name = "btnDirOut";
            this.btnDirOut.Size = new System.Drawing.Size(75, 23);
            this.btnDirOut.TabIndex = 2;
            this.btnDirOut.Text = "Dir Out";
            this.btnDirOut.UseVisualStyleBackColor = true;
            this.btnDirOut.Click += new System.EventHandler(this.btnDirOut_Click);
            // 
            // lblDirOut
            // 
            this.lblDirOut.AutoSize = true;
            this.lblDirOut.Location = new System.Drawing.Point(12, 67);
            this.lblDirOut.Name = "lblDirOut";
            this.lblDirOut.Size = new System.Drawing.Size(24, 13);
            this.lblDirOut.TabIndex = 3;
            this.lblDirOut.Text = "Out";
            // 
            // tbDirIn
            // 
            this.tbDirIn.AllowDrop = true;
            this.tbDirIn.Location = new System.Drawing.Point(47, 28);
            this.tbDirIn.Name = "tbDirIn";
            this.tbDirIn.Size = new System.Drawing.Size(144, 20);
            this.tbDirIn.TabIndex = 4;
            this.tbDirIn.DragDrop += new System.Windows.Forms.DragEventHandler(this.tbDirIn_DragDrop);
            this.tbDirIn.DragEnter += new System.Windows.Forms.DragEventHandler(this.tbDirIn_DragEnter);
            // 
            // tbDirOut
            // 
            this.tbDirOut.AllowDrop = true;
            this.tbDirOut.Location = new System.Drawing.Point(47, 64);
            this.tbDirOut.Name = "tbDirOut";
            this.tbDirOut.Size = new System.Drawing.Size(144, 20);
            this.tbDirOut.TabIndex = 5;
            this.tbDirOut.DragDrop += new System.Windows.Forms.DragEventHandler(this.tbDirOut_DragDrop);
            this.tbDirOut.DragEnter += new System.Windows.Forms.DragEventHandler(this.tbDirOut_DragEnter);
            // 
            // tbStatus
            // 
            this.tbStatus.Location = new System.Drawing.Point(15, 129);
            this.tbStatus.Multiline = true;
            this.tbStatus.Name = "tbStatus";
            this.tbStatus.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbStatus.Size = new System.Drawing.Size(257, 124);
            this.tbStatus.TabIndex = 6;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(15, 110);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(37, 13);
            this.lblStatus.TabIndex = 7;
            this.lblStatus.Text = "Status";
            // 
            // btnScrape
            // 
            this.btnScrape.Location = new System.Drawing.Point(196, 99);
            this.btnScrape.Name = "btnScrape";
            this.btnScrape.Size = new System.Drawing.Size(75, 23);
            this.btnScrape.TabIndex = 8;
            this.btnScrape.Text = "Scrape";
            this.btnScrape.UseVisualStyleBackColor = true;
            this.btnScrape.Click += new System.EventHandler(this.btnScrape_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 265);
            this.Controls.Add(this.btnScrape);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.tbStatus);
            this.Controls.Add(this.tbDirOut);
            this.Controls.Add(this.tbDirIn);
            this.Controls.Add(this.lblDirOut);
            this.Controls.Add(this.btnDirOut);
            this.Controls.Add(this.lblDirIn);
            this.Controls.Add(this.btnDirIn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main";
            this.Text = "TeaSpoon";
            this.Load += new System.EventHandler(this.Main_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog fbdDirIn;
        private System.Windows.Forms.FolderBrowserDialog fbdDirOut;
        private System.Windows.Forms.Button btnDirIn;
        private System.Windows.Forms.Label lblDirIn;
        private System.Windows.Forms.Button btnDirOut;
        private System.Windows.Forms.Label lblDirOut;
        private System.Windows.Forms.TextBox tbDirIn;
        private System.Windows.Forms.TextBox tbDirOut;
        private System.Windows.Forms.TextBox tbStatus;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Button btnScrape;
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngleSharp.Dom;
using AngleSharp.Dom.Html;
using AngleSharp.Parser.Html;
using ZetaProducerHtmlCompressor;

namespace TeaSpoon
{
    public class Parser
    {
        private string html;
        private IHtmlDocument dom;

        public string title;

        public Parser(string html)
        {
            this.html = html;
        }

        private IHtmlDocument getDom()
        {
            if(this.dom == null)
            {
                this.setDom(this.html);
            }

            return this.dom;
        }

        private void setDom(string html)
        {
            HtmlParser parser = new HtmlParser();
            this.dom = parser.Parse(html);
        }

        private string getData()
        {
            string selector = ".menu-tab-button";
            IHtmlCollection<IElement> elements = this.getDom().QuerySelectorAll(selector);

            StringBuilder sb = new StringBuilder();

            foreach (IElement h in elements)
            {
                string id = h.GetAttribute("id");
                string name = h.TextContent;

                string iselector = id + "-tab";

                sb.Append("########################\n");
                sb.Append(this.getSubText(iselector, name));
                sb.Append(this.getSubImage(iselector, name));
                sb.Append("\n\n");
            }

            return sb.ToString();
        }

        private string getSubText(string selector, string name)
        {
            StringBuilder sb = new StringBuilder();
            IElement element = this.getDom().GetElementById(selector);
            IHtmlCollection<IElement> children = element.QuerySelectorAll("[class*='sub-text']");

            foreach(IElement el in children)
            {
                if(el.InnerHtml != "")
                {
                    HtmlContentCompressor hcc = new HtmlContentCompressor();
                    sb.Append("****\n");
                    sb.Append(name + ": " + el.GetAttribute("class"));
                    sb.Append("\n\n");
                    sb.Append(hcc.Compress(el.InnerHtml));
                    sb.Append("\n\n");
                }
            }

            return sb.ToString();

            
        }

        private string getSubImage(string selector, string name)
        {
            StringBuilder sb = new StringBuilder();
            IElement element = this.getDom().GetElementById(selector);
            IHtmlCollection<IElement> children = element.QuerySelectorAll("[class*='sub-image']");

            foreach (IElement el in children)
            {
                if (el.InnerHtml != "")
                {
                    sb.Append("****\n");
                    sb.Append(name + ": " + el.GetAttribute("class"));
                    sb.Append("\n\n");
                    sb.Append(el.InnerHtml);
                }
            }

            return sb.ToString();
        }

        public string parse()
        {
            return this.getData();
        }
    }
}
